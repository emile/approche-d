#!/bin/bash

function random(){

    nano=$(date +%N)
    rdm=${nano:((${#nano} - $1)):$1}
    
    while [[ $rdm > $2 ]]; do
        nano=$(date +%N)
        rdm=${nano:((${#nano} - $1)):$1}
    done

    echo $rdm
}

chin=$(cat txt/chin/$(random 1 7).txt)
hair=$(cat txt/hair/$(random 1 4).txt)
sexe=$(random 1 1)

if [ $sexe == 1 ]; then
    # female
    pilosity=$(cat txt/pilosity/0.txt)
else
    # male
    pilosity=$(cat txt/pilosity/$(random 1 4).txt)
fi

nose=$(cat txt/nose/$(random 1 7).txt)

portrait=""

for ((i=0; i<${#chin}; i++)); do
    part="${chin:$i:1}"
    if [[ ${hair:$i:1} != ' ' ]]; then
        part="${hair:$i:1}"
    fi
    if [[ ${pilosity:$i:1} != ' ' ]]; then
        part="${pilosity:$i:1}"
    fi
    if [[ ${nose:$i:1} != ' ' ]]; then
        part="${nose:$i:1}"
    fi
    portrait="$portrait$part"
done

echo "$portrait"
