Etude de personnages                                                         1/7


 ___ ___ _ _ __  ___    __  ___    ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ ___ 
| __|_ _| | |  \| __|  |  \| __|  | _ | __| _ | __|   |   |   | _ | __| __| __|
| __|| || | | | | __|  | | | __|  |  _| __|  _|__ | | | | | | | _ | | | __|__ |
|___||_||___|__/|___|  |__/|___|  |_| |___|_\_\___|___|_|_|_|_|_|_|___|___|___|
Traduire le corps en glyphes                                           mxb 2019


Index
-----

    Introduction ------------------------------------------------------------- 1
  I Corps -------------------------------------------------------------------- 2
 II Expressions -------------------------------------------------------------- 4
III Machoire ----------------------------------------------------------------- 5
 IV Cheveux ------------------------------------------------------------------ 5
  V Pilosite ----------------------------------------------------------------- 5
 VI Nez ---------------------------------------------------------------------- 6
VII Portraits ---------------------------------------------------------------- 6


Introduction
------------

En produisant ces dessins je pense au corps comme signe. L'ASCII permet ici de
mettre en evidence le travail de rationalisation de la representation du corps.
Un lieu commun des planches anatomiques, des "story-board" et du "character
design". Des signes faits de signes permettant de faire d'autres signes.

La partie VII contient des portraits produits par un script bash qui accompagne
ce fichier. Ce dernier assemble aleatoirement des dessins des parties II, III, 
IV et V. Pour l'utiliser ouvrez un invite de commande ou un terminal sur un 
systeme unix, placez vous dans le dossier :

    cd "path/to/directory"/ascii_charater_study

permettez l'exectution du script

    chmod +x portrait_generator.sh

et executez le script

    ./portrait_generator.sh

Le portait apparait alors sous la forme textuelle d'un "standard output", il est
possible le rediriger vers un autre fichier.

    ./portait_generator > new_portrait.txt









dng                                                                      09/2019

Etude de personnages                                                         2/7


Corps 
----- 





                                    .-~~~-.             
                                   /       \            
                                   ! _   _ !            
                                  (. 0   0 .)           
                                   !   L   !            
                                    \  -  /             
                                    !`-.-'!             
                                   /'     `\            
                              _.-~' ._\ /_. `~-._       
                            .'        `.'        `.     
                          .'          \ /          `.   
                          !    )               (    !   
                         .'   ||               ||   `.  
                         |    !!      .~.      !!    |  
                         !   .' \             / `.   !  
                        .'   !   )  .  !  .  (   !   `. 
                        .\  /   /      !      \   \  /. 
                        |   |  (               )  |   | 
                        \   |  !\             /!  |   / 
                        `.  ! .' ` .       . ' `. !  .' 
                         | .'.'      `. .'      `.`. |  
                         | ! |         Y         | ! |  
                         !  )|         !         |(  !  
                          \()!        / \        !()/   
                           \)`.       ! !       .'(/    
                              `.       Y       .'       
                               |       |       |        
                               !       |       !        
                               `.(  ) .~. (  ).'        
                                |`\  '. .`  /'|         
                                |     | |     |         
                                !     | |     !         
                                `\    | |    /'         
                                 |    ! !    |          
                                 |   .' `.   |          
                                 !   |   |   !          
                                 `.  |   |  .'          
                                  !  !   !  !           
                                  /   ) (   \           
                                .'    ) (    `.         
                               ((((o(o) (o)o))))        

                                                    






dng                                                                      09/2019

Etude de personnages                                                         3/7








                   ___                                   ___  
                 /(|||)\                               /(|||)\                
                (//'-`\\)                             (//'-`\\)               
                ! _   _ !                             ! _   _ !               
               (. 0   0 .)                           (. 0   0 .)              
                !   L   !                             !   L   !               
                 \  -  /                               \  -  /                
                 !`-.-'!                               !`-.-'!                
                /'     `\                             /'     `\               
           _.-~' ._\ /_. `~-._                   _.-~' ._\ /_. `~-._          
         .'        `.'        `.               .'        `.'        `.        
       .'          \ /          `.           .'          \ /          `.      
       !    ) .           . (    !           !    )               (    !      
      .'   ( (o)  /   \  (o) )   `.         .'   || (o         o) ||   `.     
      |    !`.__.' .~. `.__.'!    |         |    !!      .~.      !!    |     
      !   .' \             / `.   !         !   .':               :`.   !     
     .'   !   )  .  !  .  (   !   `.       .'   !  \   .  !  .   /  !   `.    
     .\  /   .'     *     `.   \  /.       .\  /   !      *      !   \  /.    
     |   |  /              '\  |   |       |   |  .'             `.  |   |    
     \   | ! \      .      / ! |   /       \   |  !\      .      /!  |   /    
     `.  !.'  ` .  )))  . '  `.!  .'       `.  ! .' ` .  )))  . ' `. !  .'    
      | .'|       ((((        |`. |         | .' |      ((((       | `. |     
      | ! |         W         | ! |         | !  |       ( )       |  ! |     
      !  )!         !         !(  !         !  ) |        U        | (  !     
       \()`.       / \       .'()/           \() |       / \       | ()/      
        \) !       ! !       ! (/             \) !       ! !       ! (/       
           `.       Y       .'                   `.       Y       .'          
            |       |       |                     |       |       |           
            !       |       !                     !       |       !           
            `.(  ) .~. (  ).'                     `.(  ) .~. (  ).'           
             |`\  '. .`  /'|                       |`\  '. .`  /'|            
             |     | |     |                       |     | |     |            
             !     | |     !                       !     | |     !            
             `\    | |    /'                       `\    | |    /'            
              |    ! !    |                         |    ! !    |             
              |   .' `.   |                         |   .' `.   |             
              !   |   |   !                         !   |   |   !             
              `.  |   |  .'                         `.  |   |  .'             
               !  !   !  !                           !  !   !  !              
               /   ) (   \                           /   ) (   \              
             .'    ) (    `.                       .'    ) (    `.            
            ((((o(o) (o)o))))                     ((((o(o) (o)o))))           








dng                                                                      09/2019

Etude de personnages                                                         4/7


Expressions       
-----------        

    Serieux.se  Concentre.e Surpris.e   Decu.e      Parle       Choque.e    
      .-~~~-.     .-~~~-.     .-~~~-.     .-~~~-.     .-~~~-.     .-~~~-.   
     /       \   /       \   /       \   /       \   /       \   /       \  
     ! _   _ !   ! _   _ !   ! -   - !   ! -   - !   ! _   _ !   ! -   - !  
    (. 0   0 .) (. 0   0 .) (. 0   0 .) (. 0   0 .) (. 0   0 .) (. 0   0 .)  
     !   L   !   !   L   !   !   L   !   !   L   !   !   L   !   !   L   !  
      \  -  /     \  _  /     \  -  /     \  _  /     \  O  /     \  O  /   
      !`-.-'!     !`-.-'!     !`-.-'!     !`-.-'!     !`-.-'!     !`-.-'!   
                                                                            
    Pense       Somnolent.e Medite      Dort        Baille      Ronfle      
      .-~~~-.     .-~~~-.     .-~~~-.     .-~~~-.     .-~~~-.     .-~~~-.   
     /       \   /       \   /       \   /       \   /       \   /       \  
     ! _   _ !   ! _   _ !   ! -   - !   ! -   - !   ! _   _ !   ! -   - !  
    (. -   - .) (. -   - .) (. -   - .) (. -   - .) (. -   - .) (. -   - .) 
     !   L   !   !   L   !   !   L   !   !   L   !   !   L   !   !   L   !  
      \  -  /     \  _  /     \  -  /     \  _  /     \  O  /     \  O  /   
      !`-.-'!     !`-.-'!     !`-.-'!     !`-.-'!     !`-.-'!     !`-.-'!   
                                                                            
    Fier.e      Heureux.se  Approuve    Plaisir     Domine      Moque       
      .-~~~-.     .-~~~-.     .-~~~-.     .-~~~-.     .-~~~-.     .-~~~-.   
     /       \   /       \   /       \   /       \   /       \   /       \  
     ! _   _ !   ! -   - !   ! _   _ !   ! -   - !   ! \   / !   ! \   / !  
    (. 0   0 .) (. 0   0 .) (. -   - .) (. -   - .) (. 0   0 .) (. -   - .) 
     !   L   !   !   L   !   !   L   !   !   L   !   !   L   !   !   L   !  
      \ `-' /     \ `-' /     \ `-' /     \ `-' /     \ `-' /     \ `-' /   
      !`-.-'!     !`-.-'!     !`-.-'!     !`-.-'!     !`-.-'!     !`-.-'!   
                                                                            
    Attendri.e  Blague      Triste      Suplie      Deprime     Fatigue     
      .-~~~-.     .-~~~-.     .-~~~-.     .-~~~-.     .-~~~-.     .-~~~-.   
     /       \   /       \   /       \   /       \   /       \   /       \  
     ! /   \ !   ! /   \ !   ! /   \ !   ! /   \ !   ! /   \ !   ! /   \ !  
    (. 0   0 .) (. -   - .) (. 0   0 .) (. 0   0 .) (. -   - .) (. -   - .) 
     !   L   !   !   L   !   !   L   !   !   L   !   !   L   !   !   L   !  
      \ `-' /     \ `-' /     \  -  /     \  _  /     \  -  /     \  _  /   
      !`-.-'!     !`-.-'!     !`-.-'!     !`-.-'!     !`-.-'!     !`-.-'!   
                                                                            
    Peur        Pleure      Colere      Rage        Cri.e       Souffre     
      .-~~~-.     .-~~~-.     .-~~~-.     .-~~~-.     .-~~~-.     .-~~~-.   
     /       \   /       \   /       \   /       \   /       \   /       \  
     ! /   \ !   ! /   \ !   ! \   / !   ! \   / !   ! \   / !   ! \   / !  
    (. 0   0 .) (. -   - .) (. 0   0 .) (. 0   0 .) (. 0   0 .) (. -   - .) 
     !   L   !   !   L   !   !   L   !   !   L   !   !   L   !   !   L   !  
      \  O  /     \  O  /     \  -  /     \  _  /     \  O  /     \  O  /   
      !`-.-'!     !`-.-'!     !`-.-'!     !`-.-'!     !`-.-'!     !`-.-'!   
                                                                            
    Refuse      Frustre.e   
      .-~~~-.     .-~~~-.   
     /       \   /       \  
     ! \   / !   ! \   / !  
    (. -   - .) (. -   - .) 
     !   L   !   !   L   !  
      \  -  /     \  _  /   
      !`-.-'!     !`-.-'!   
dng                                                                      09/2019

Etude de personnages                                                         5/7
        

Machoire
--------

                                                                             
        .-~~~-.            .-~~~-.            .-~~~-.            .-~~~-.     
       /       \          /       \          /       \          /       \    
       ! _   _ !          ! _   _ !          ! _   _ !          ! _   _ !     
      (. 0   0 .)        (. 0   0 .)        (. 0   0 .)        (. 0   0 .)    
        \  L  /           !   L   !          !   L   !          !   L   !     
        |\ - /|            \  -  /            \  -  /            \  -  /      
        ! `-' !            !`-.-'!            !`._.'!            !\._./!      
       /'     `\          /'     `\          /'     `\          /'     `\     
   .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-. 
   
                                                                             
        .-~~~-.            .-~~~-.            .-~~~-.            .-~~~-.     
       /       \          /       \          /       \          /       \    
       ! _   _ !          ! _   _ !          ! _   _ !          ! _   _ !     
      (. 0   0 .)        (. 0   0 .)        (. 0   0 .)        (. 0   0 .)    
       |   L   |          |   L   |          /   L   \          |   L   |     
       !   -   !          !   -   !          \   -   /          !   -   !     
        !.___.!            \.___./            !.___.!           (       )     
       /'     `\          /'     `\          /'     `\          /`~---~'\     
   .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.


Cheveux 
------- 

          ___               _..._              _..._             _.---._     
        /(|||)\           .~(|||)~.          (((|||)))         .(((|||))).   
       (//'~`\\)         ((//'~`\\))        ((//'~`\\))       ((((/'~`\))))  
       ! _   _ !         (! _   _ !)       ((! _   _ !))      ))!'_   _`!((  
      (. 0   0 .)        (. 0   0 .)       ((. 0   0 .))     (((. 0   0 .))) 
       !   L   !          !   L   !         )!   L   !(       )`!   L   !'(  
        \  -  /            \  -  /          ((\  -  /))      (((`\  -  /'))) 
        !`-.-'!            !`-.-'!            !`-.-'!         )))!`-.-'!(((  
       /'     `\          /'     `\          /'     `\       (((/'     `\))) 
   .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.   
  

Pilosite
--------
                                                                             
        .-~~~-.            .-~~~-.            .-~~~-.            .-~~~-.     
       /       \          /       \          /       \          /       \    
       ! _   _ !          ! _   _ !          ! _   _ !          ! _   _ !    
      (. 0   0 .)        (. 0   0 .)        (. 0   0 .)        (. 0   0 .)   
       !   L   !          !  .L.  !          ! _.L._ !          (\_.L._/)    
        \ '-` /            \(/-\)/            (((-)))           ((((-))))    
        !`-.-'!            !`-.-'!            !\(|)/!            (((|)))     
       /'     `\          /'     `\          /'     `\          /'(|||)`\    
   .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.  .-~' ._(|)_. `~-.



dng                                                                      09/2019

Etude de personnages                                                         6/7


Nez 
---

                                                                             
        .-~~~-.            .-~~~-.            .-~~~-.            .-~~~-.     
       /       \          /       \          /       \          /       \    
       ! _   _ !          ! _   _ !          ! _   _ !          ! _   _ !    
      (. 0   0 .)        (. 0   0 .)        (. 0   0 .)        (. 0   0 .)   
       !   L   !          !   <   !          !   c   !          !   V   !    
        \  -  /            \  -  /            \  -  /            \  -  /     
        !`-.-'!            !`-.-'!            !`-.-'!            !`-.-'!     
       /'     `\          /'     `\          /'     `\          /'     `\    
   .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.
   
                                                                             
        .-~~~-.            .-~~~-.            .-~~~-.            .-~~~-.     
       /       \          /       \          /       \          /       \    
       ! _   _ !          ! _   _ !          ! _   _ !          ! _   _ !     
      (. 0   0 .)        (. 0   0 .)        (. 0   0 .)        (. 0| |0 .)    
       !   J   !          !   U   !          !  (_)  !          !  (_)  !     
        \  -  /            \  -  /            \  -  /            \  -  /      
        !`-.-'!            !`-.-'!            !`-.-'!            !`-.-'!      
       /'     `\          /'     `\          /'     `\          /'     `\     
   .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.


Portraits                   
---------                   

         ___                                   ___                ___       
       /(|||)\            .-~~~-.            /(|||)\            /(|||)\     
      (//'~`\\)          /       \          (//'~`\\)          (//'~`\\)    
      ! _   _ !          ! _   _ !          ! _   _ !          ! _   _ !    
     (. 0   0 .)        (. 0   0 .)        (. 0   0 .)        (. 0   0 .)   
       \  c  /           |  .J.  |          (\_.U._/)          |   U   |    
       |\ - /|           ! (/-\) !          (((/-\)))          !  '-`  !    
       ! `-' !            \.___./            (((|)))            \.___./     
      /'     `\          /'     `\          /'(|||)`\          /'     `\    
  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.  .-~' ._(|)_. `~-.  .-~' ._\ /_. `~-.
         ___               _..._              _..._              _..._      
       /(|||)\           .~(|||)~.          (((|||)~.          (((|||)~.    
      (//'~`\\)         ((//'~`\\))        ((//'~`\\))        ((//'~`\\))   
      ! _   _ !         (! _   _ !)       ((! _   _ !))      ((! _   _ !))  
     (. 0   0 .)        (. 0   0 .)       ((. 0   0 .))      ((. 0   0 .))  
      !  .L.  !           \ .c. /          )!   c   !(        )!   V   !(   
       \(/-\)/            |(/-\)|          ((\  -  /))        ((\  -  /))   
       !\._./!            ! `-' !            !`-.-'!            !`-.-'!     
      /'     `\          /'     `\          /'     `\          /'     `\    
  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.







dng                                                                      09/2019

Etude de personnages                                                         7/7


        _..._              _..._              _..._              _..._      
      (((|||)~.          .~(|||)~.          .~(|||)~.         .(((|||))).   
     ((//'~`\\))        ((//'~`\\))        ((//'~`\\))       ((((/'~`\))))  
    ((! _   _ !))       (! _   _ !)        (! _   _ !)       ))!'_   _`!((  
    ((. 0| |0 .))       (. 0   0 .)        (. 0   0 .)      (((. 0   0 .))) 
     )!  (_)  !(         ! _.V._ !          !  (_)  !        ))!   L   !((  
     ((\  -  /))          ((/-\))            \  -  /        ((((\  -  /)))) 
       !`._.'!            !\(|)/!            !\._./!         )))!`-.-'!(((  
      /'     `\          /'     `\          /'     `\       (((/'     `\))) 
  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.
         ___               _..._              _..._               ___       
       /(|||)\           (((|||)~.         .(((|||))).          /(|||)\     
      (//'~`\\)         ((//'~`\\))       ((((/'~`\))))        (//'~`\\)    
      ! _   _ !        ((! _   _ !))      ))!'_   _`!((        ! _   _ !    
     (. 0   0 .)       ((. 0   0 .))     (((. 0   0 .)))      (. 0   0 .)   
      |   U   |         )|  .V.  |(       ))/  .V.  \((        !   V   !    
      !  '-`  !         (( (/-\) ))      (((( (/-\) ))))        \ '-` /     
       !.___.!            \.___./         )))!.___.!(((         !`._.'!     
      /'     `\          /'     `\       (((/'     `\)))       /'     `\    
  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.
                            ___               _..._               ___       
       .-~~~-.            /(|||)\          .(((|||))).          /(|||)\     
      /       \          (//'~`\\)        ((((/'~`\))))        (//'~`\\)    
      ! _   _ !          ! _   _ !        ))!'_   _`!((        ! _   _ !    
     (. 0   0 .)        (. 0   0 .)      (((. 0| |0 .)))      (. 0   0 .)   
      !   L   !          |   U   |        )) \ (_) / ((        / _(_)_ \    
       \  -  /           !   -   !       ((((|\ - /|))))       \((/-\))/    
       !\._./!           (       )        )))! `-' !(((         !\(|)/!     
      /'     `\          /`~---~'\       (((/'     `\)))       /'     `\    
  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.
        _..._                                 _..._              _..._      
      .~(|||)~.           .-~~~-.           (((|||)~.         .(((|||))).   
     ((//'~`\\))         /       \         ((//'~`\\))       ((((/'~`\))))  
     (! _   _ !)         ! _   _ !        ((! _   _ !))      ))!'_   _`!((  
     (. 0   0 .)        (. 0   0 .)       ((. 0   0 .))     (((. 0   0 .))) 
       \ .U. /           |   J   |         )!   L   !(       ))(\_.<._/)((  
       |(/-\)|           !   -   !         ((\  -  /))      ((((((/-\)))))) 
       ! `-' !            !.___.!            !`-.-'!         )))(((|)))(((  
      /'     `\          /'     `\          /'     `\       (((/'(|||)`\))) 
  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.  .-~' ._(|)_. `~-.
                            ___               _..._                         
       .-~~~-.            /(|||)\           (((|||)~.           .-~~~-.     
      /       \          (//'~`\\)         ((//'~`\\))         /       \    
      ! _   _ !          ! _   _ !        ((! _   _ !))        ! _   _ !    
     (. 0   0 .)        (. 0   0 .)       ((. 0   0 .))       (. 0   0 .)   
      | _.J._ |          !  (_)  !         )|  .c.  |(         |   L   |    
      !((/-\))!           \  -  /          (( (/-\) ))         !   -   !    
      ( \(|)/ )           !`._.'!            \.___./           (       )    
      /`~---~'\          /'     `\          /'     `\          /`~---~'\    
  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.  .-~' ._\ /_. `~-.






dng                                                                      09/2019
