ASCII pour Approche                                                          1/4


Index
-----

[dir] FIGschiff                 une FIGfont fraktur en caracteres ASCII
[dir] etude_de_personnages      traduire le corps en glyphes
[dir] jgs                       une font special art ASCII
      LOOKATME.pdf              cette page (print)
      README.txt                cette page (ASCII)
      RUNME.php                 cette page (web)
[dir] src                       fichiers de style et fonts

Vous pouvez retrouver ces fichiers sur https://framagit.org/emile/approche-d


Introduction
------------

Bonjour Approche,

pour repondre a votre thematique nous vous joignons un dossier sur l'art ASCII,
une pratique qui occupe une place importante dans notre amitie et nos travaux.

L'art ASCII etait autrefois le copain de l'utilisateur.

Pour nous l'art ASCII represente un objet critique de design. Un objet technique
leger et perenne, une pratique amatrice du detournement qui ne pretend pas a
plaire ou imposer son ordre. L'art ASCII privilegie l'interpretation a la
lisibilite et fait avec l'existant sans exiger de moyen technique nouveau. C'est
un art souterrain, heritier d'une longue histoire de la machine a ecrire, qui se
place dans les interstices de la culture strategique des interfaces. Un art
populaire contraint a l'invisibilite depuis plus d'une decennie : bien que le
standard ASCII soit encore aux fondemements du fonctionnement de nos
ordinateurs, la disparition du code des ecrans de nos machines, la fin des
interfaces en mode texte, de l'internet des pages personnelles et celle plus
progressive du clavier d'ordinateur servent a la disparition de cette pratique.
Dans ce contexte et en tant que graphistes impregnes de la culture du texte et
de l'ecrit, l'art ASCII apparait comme une pratique etonnament peu mise en
valeur par le monde du design graphique.

Pour finir cette introduction voici une traduction libre et tres incomplete
d'une FAQ sur l'art ASCII signee Mathew Thomas probablement publiee en 1998.















dng                                                                      09/2019

ASCII pour Approche                                                          2/4


FAQ
---

Qu'est ce que l'art ASCII ?

    Est "art ASCII" toute forme illustrative -- image, graphique, BD, autre
-- dessine avec les glyphs presents dans le standard ASCII (Basic Latin dans
l'unicode).

    Le standard ASCII (American Standard Code for Information Interchange ou
standard americain pour l'echange d'informations) comporte 128 entrees
(indexees de 0 a 127) qui sont comprises par la quasi totalite des ordinateurs.
Les seuls entrees representant les glyphes utilises pour l'art ASCII sont entre
les valeures 32 et 126 (ci-dessous). Les autres valeures representent des trucs
du genre "fin de fichier", "retour", "nouvelle ligne".

    032 [space] 048 0       064 @       080 P       096 `       112 p
    033 !       049 1       065 A       081 Q       097 a       113 q
    034 "       050 2       066 B       082 R       098 b       114 r
    035 #       051 3       067 C       083 S       099 c       115 s
    036 $       052 4       068 D       084 T       100 d       116 t
    037 %       053 5       069 E       085 U       101 e       117 u
    038 &       054 6       070 F       086 V       102 f       118 v
    039 '       055 7       071 G       087 W       103 g       119 w
    040 (       056 8       072 H       088 X       104 h       120 x
    041 )       057 9       073 I       089 Y       105 i       121 y
    042 *       058 :       074 J       090 Z       106 j       122 z
    043 +       059 ;       075 K       091 [       107 k       123 {
    044 ,       060 <       076 L       092 \       108 l       124 |
    045 -       061 =       077 M       093 ]       109 m       125 }
    046 .       062 >       078 N       094 ^       110 n       126 ~
    047 /       063 ?       079 O       095 _       111 o

    Ces glyphes sont presques completement standards, exepte pour quelques
petites exeptions a garder en tete quand il s'agit d'afficher ou de dessiner de
l'art ASCII :


    # (hashtag ou diese):
        un hashtag sur la pluspart des ordinateur, un signe du livre (monnaie) 
        sur quelques machines Britanniques

    | (barre):
        une barre vertical dans la pluspart des fonts mais dont le tiers du
        milieu est retiré sur quelques unes

    ^ (caret):
        la taille de ce glyphe peut beaucoup differer selon les fonts
    
    ~ (tilde):
        apparait au milieu de la ligne sur certaines fonts, en heut sur d'autres
    
    ' (apostrophe/single quote):
        comme la virgule ce glyphe est dessine en diagonal sudouest-nordest sur
        certaines fonts, verical sur d'autres

dng                                                                      09/2019

ASCII pour Approche                                                          3/4


    Voici un petit example d'art ASCII utilisant des glyphes variables. Dessine
par Joan G. Stark, son apparence depend de la font, du systeme d'exploitation
et du logiciel utilise pour l'afficher.

                              
                              



















                                      ____
                                   .-" +' "-.
                                  /.'.'A_'*`.\
                                 |:.*'/\-\. ':|                                 
                                 |:.'.||"|.'*:|
                                  \:~^~^~^~^:/
                                   /`-....-'\
                              jgs /          \
                                  `-.,____,.-'






















dng                                                                      09/2019

ASCII pour Approche                                                          4/4


    Les gens qui pratique l'art ASCII ont presque tous une bonne raison de le
faire, quelques exemples :

 
    * C'est la forme d'art la plus universelle qui soit -- n'importe quel
      systeme informatique capable d'afficher des lignes de texte peut afficher
      de l'art ASCII, sans necessite un mode graphique ou de comprendre un
      format de fichier particulier;

    * Une image en ASCII est une centaine de fois plus legere en terme de poid
      de fichier qu'une image en GIF ou JPG, BMP d'une complexite equivalente.
    
    * C'est facile a copier, assembler, coller d'un fichier a un autre

    * C'est fun a faire !









































dng                                                                      09/2019
