# Schiff

Schiff est une FIGfont fraktur en caractères ASCII inspiré par la couverture du
livre *Die Schiffs Fibel* (voir resources/die-schiffs-fibel) et destiné à être
utilisé avec le programme FIGlet.

FIGlet (<http://www.figlet.org>) est un programme qui permet de créer des
bannières textuelles dans différentes polices d'écriture (FIGfonts). Chaque
caractère (FIGcharacter) est composé d'un ensemble de sous-caractère (souvent en
ASCII). Le nom dérive des lettres de "Frank", "Ian" et "Glenn". 

>Au printemps 1991, inspiré par la signature électronique de Frank Sheeran et
>poussé par mon bon ami Ian Chai, j'ai (Glenn Chappell) écrit un petit programme
>"C" de 170 lignes que j'ai appelé "newban". Avec le recul, nous l’appelons
>maintenant «FIGlet 1.0» et, à diverses reprises, il a circulé sur le réseau
>pendant quelques années. Il y avait une police, qui ne comprenait que des
>lettres minuscules. [...]

Glen Chappell, 1995, <http://www.figlet.org/figlet_history.html>
